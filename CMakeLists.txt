#******************************************************************
#**
#**  php5-fcgi master build script
#**
#******************************************************************
#** 
#** Copyright (C) 2012 Snowball Factory Inc.
#** 
#******************************************************************

# we need cmake 2.8.3
cmake_minimum_required(VERSION 2.8.3)

# setup our package info
SET(ANTIPASTI_NAME "php5-fastcgi")
SET(ANTIPASTI_ARCH_ALL 1)
SET(ANTIPASTI_DEPENDS "php5-cgi, dpkg, psmisc, coreutils, sysv-rc, lsb-base, initscripts")
SET(ANTIPASTI_CONTROL_FILES "postinst" "prerm")
SET(ANTIPASTI_CONTACT "Awe.sm Packages <packages@awe.sm>")
SET(ANTIPASTI_DESCRIPTION "php5 fcgi configuration
 Setup for php5 fastcgi.")

# setup our files to install
SET(CMAKE_INSTALL_PREFIX "/")
INSTALL(PROGRAMS
	php-fastcgi
	DESTINATION "etc/init.d")
INSTALL(FILES
	copyright
	DESTINATION "usr/local/share/doc/${ANTIPASTI_NAME}")

# include antipasti
INCLUDE(Antipasti)

